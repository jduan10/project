/*
    Name: Jiaqi Duan
    CruzID: jduan10
 
    Source file Name: List.h
    IDE: XCode
*/

# ifndef List_h
# define List_h

/* ------------------------------  Structs  ----------------------------------*/

typedef struct ListObj* List;

/* ----------------------  Constructors-Destructors --------------------------*/

List newList(void);
void freeList(List* pL);

/* --------------------------- Access functions -------------------------------*/

int length(List L);
int indexElem(List L);
int front(List L);
int back(List L);
int get(List L);
int equals(List A, List B);
int isEmpty(List L);

/* ------------------------ Manipulation procedures ----------------------------*/

void clearList(List L);
void set(List L, int x);
void moveFront(List L);
void moveBack(List L);
void movePrev(List L);
void moveNext(List L);
void prepend(List L, int x);
void append(List L, int x);
void insertBefore(List L, int x);
void insertAfter(List L, int x);
void deleteFront(List L);
void deleteBack(List L);
void delete(List L);

/* ----------------------------- Other operations -------------------------------*/
void printList(FILE* out, List L);
List copyList(List L);
List concatList(List A, List B);

# endif /* List_h */
